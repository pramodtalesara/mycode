import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class VariousSortingAlgo {

    public static void main(String[] args) {

        quickSort();
      //  String[] array = { "A", "B", "C", "D"  };

 /*     IntStream.range(2, 4)
                .mapToObj(index -> String.format("%d -> %s", index, array[index]))
                .forEach(System.out::print);
*/
    }


    public static void quickSort() {

        Random r = new Random();

        int[] myArray = IntStream.generate(() -> r.nextInt(1000000)).limit(100000000).toArray();

       // System.out.println(myArray);


       // System.out.println("before sorting");

        //System.out.println();
      /*  IntStream.range(0, myArray.length)
                .mapToObj(index ->  " " + myArray[index] + " ")
                .forEach(System.out::print);
*/
        long start = System.currentTimeMillis();
      //quickSort(myArray,0,myArray.length-1);
       // mergeSort(myArray,myArray.length-1);
       //dualPivotQuickSort(myArray,0,myArray.length-1);
      //insurtionSort(myArray);
      // sort(myArray);

     Arrays.parallelSort(myArray);

   //  Arrays.sort(myArray);

        long end = System.currentTimeMillis();


        System.out.println("aftere sorting");
        /*IntStream.range(0, myArray.length)
                .mapToObj(index ->  " " + myArray[index] + " ")
                .forEach(System.out::print);*/

     long time = (end-start)/1000;
        System.out.println();
        System.out.println("time : " + start);
        System.out.println("time : " + end);
        System.out.println("time : " + time);
        //  Arrays.stream(myArray).forEach((a)-> System.out.print(" " + a));
       // System.out.println();

    }



    public static void quickSort(int arr[], int begin, int end) {

        if (begin < end) {
            int partitionIndex = partition(arr, begin, end);

            quickSort(arr, begin, partitionIndex-1);
            quickSort(arr, partitionIndex+1, end);
        }
    }


    public static int partition(int arr[], int begin, int end) {
        int pivot = arr[end];
        int i = (begin-1);
       // System.out.println("begin " + begin + " , end "+ end);
        for (int j = begin; j < end; j++) {
            if (arr[j] <= pivot) {
                i++;
               /* System.out.println("Found " + arr[j] + " smaller than " + pivot + " so moving " + arr[j] + " to left and increase smaller number counter to " + i);
                System.out.println("Moved" + arr[j] + " to the positio of " + i + " where " + arr[i] + " is currently stored");*/
                int swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;

/**/



              /*  IntStream.range(begin, end+1)
                        .mapToObj(index ->  " " + arr[index] + " ")
                        .forEach(System.out::print);*/
               // System.out.println();
            }
        }

        int swapTemp = arr[i+1];
        arr[i+1] = arr[end];
        arr[end] = swapTemp;
      //  System.out.println("Final aftere iteration");
       /* IntStream.range(begin, end+1)
                .mapToObj(index ->  " " + arr[index] + " ")
                .forEach(System.out::print);*/
       // System.out.println();
        return i+1;
    }


    public static void mergeSort(int[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, n - mid);

        merge(a, l, r, mid, n - mid);
    }


    public static void merge(
            int[] a, int[] l, int[] r, int left, int right) {

        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                a[k++] = l[i++];
            }
            else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }




    static void dualPivotQuickSort(int[] arr,
                                   int low, int high)
    {
        if (low < high)
        {

            // piv[] stores left pivot and right pivot.
            // piv[0] means left pivot and
            // piv[1] means right pivot
            int[] piv;
            piv = partitionDP(arr, low, high);

            dualPivotQuickSort(arr, low, piv[0] - 1);
            dualPivotQuickSort(arr, piv[0] + 1, piv[1] - 1);
            dualPivotQuickSort(arr, piv[1] + 1, high);
        }
    }

    static int[] partitionDP(int[] arr, int low, int high)
    {
        if (arr[low] > arr[high])
            swap(arr, low, high);

        // p is the left pivot, and q
        // is the right pivot.
        int j = low + 1;
        int g = high - 1, k = low + 1,
                p = arr[low], q = arr[high];

        while (k <= g)
        {

            // If elements are less than the left pivot
            if (arr[k] < p)
            {
                swap(arr, k, j);
                j++;
            }

            // If elements are greater than or equal
            // to the right pivot
            else if (arr[k] >= q)
            {
                while (arr[g] > q && k < g)
                    g--;

                swap(arr, k, g);
                g--;

                if (arr[k] < p)
                {
                    swap(arr, k, j);
                    j++;
                }
            }
            k++;
        }
        j--;
        g++;

        // Bring pivots to their appropriate positions.
        swap(arr, low, j);
        swap(arr, high, g);

        // Returning the indices of the pivots
        // because we cannot return two elements
        // from a function, we do that using an array.
        return new int[] { j, g };
    }

    static void swap(int[] arr, int i, int j)
    {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }


   static void insurtionSort(int arr[])
    {
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;

            /* Move elements of arr[0..i-1], that are
               greater than key, to one position ahead
               of their current position */
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;

            /*IntStream.range(0, arr.length)
                    .mapToObj(index ->  " " + arr[index] + " ")
                    .forEach(System.out::print);
            System.out.println();*/
        }
    }

    public static  void sort(int arr[])
    {
        int n = arr.length;

        // Build heap (rearrange array)
        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(arr, n, i);

        // One by one extract an element from heap
        for (int i = n - 1; i > 0; i--) {
            // Move current root to end
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;

            // call max heapify on the reduced heap
            heapify(arr, i, 0);
        }
    }

    // To heapify a subtree rooted with node i which is
    // an index in arr[]. n is size of heap
    static void heapify(int arr[], int n, int i)
    {
        int largest = i; // Initialize largest as root
        int l = 2 * i + 1; // left = 2*i + 1
        int r = 2 * i + 2; // right = 2*i + 2

        // If left child is larger than root
        if (l < n && arr[l] > arr[largest])
            largest = l;

        // If right child is larger than largest so far
        if (r < n && arr[r] > arr[largest])
            largest = r;

        // If largest is not root
        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;

            // Recursively heapify the affected sub-tree
            heapify(arr, n, largest);
        }
    }



}



