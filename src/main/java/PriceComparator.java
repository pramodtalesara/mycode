import java.util.Comparator;

public class PriceComparator implements Comparator <Order>{


    public int buycompare(Order first, Order sec){
        if (first.getPrice()>sec.getPrice())
            return -1;
        if (first.getPrice()< sec.getPrice())
            return 1;
        if (first.getPrice()==sec.getPrice())
            return first.getTime().compareTo(sec.getTime());
        else
            return 0;
    }


    public int Sellcompare(Order first, Order sec){
        if (first.getPrice()<sec.getPrice())
            return -1;
        if (first.getPrice()> sec.getPrice())
            return 1;
        if (first.getPrice()==sec.getPrice())
            return first.getTime().compareTo(sec.getTime());
        else
            return 0;
    }


    @Override
    public int compare(Order o1, Order o2) {
        if (o1.isBuyOrSell())
            return buycompare(o1,o2);
            else
                return Sellcompare(o1,o2);
    }
}
