import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import static java.util.stream.Collectors.*;

import java.util.stream.Stream;

class MyThread implements Runnable {

    @Override
    public void run()  {
        throw new NullPointerException();
    }
}

class AppleProduct implements Serializable {

    private static final long serialVersionUID = 1234567L;

    public String headphonePort;
    public String thunderboltPort;

    public String getHeadphonePort() {
        return headphonePort;
    }

    public String getThunderboltPort() {
       return  thunderboltPort;    }
}

public class MyApp {
    public static PriorityQueue<Order> OrderQueue = new PriorityQueue<Order>(400,new PriceComparator());
    public static PriorityQueue<Order> buyPQueue = new PriorityQueue<Order>(400,new PriceComparator());
    public static PriorityQueue<Order> sellPQueue = new PriorityQueue<Order>(400,new PriceComparator());
    public static List buyOrdersList;
    public static List sellOrderrsList;
    public static Semaphore buySynch = new Semaphore(1,true);
    public static Semaphore sellSynch = new Semaphore(1,true);
    public static ReentrantLock buylock = new ReentrantLock();
    public static Condition cond1 = buylock.newCondition();
    public static Condition cond2 = buylock.newCondition();
    public static Condition cond3 = buylock.newCondition();
    public static Condition cond4 = buylock.newCondition();
    public static ReentrantLock selllock = new ReentrantLock();
    public static volatile AtomicInteger buyerCounter= new AtomicInteger();
    public static volatile AtomicInteger sellerCounter=new AtomicInteger();
    public static volatile boolean tracker = true;



    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Runnable orderReceiver = ()-> {


              while(true){
                 // System.out.println("Going to acquir lock now to get orders");

                  buylock.lock();
                  try {
                      cond4.await();
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }

                  //System.out.println("I now have the lock");

                 /* try {
                      cond1.await();
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }*/
                //  System.out.println("Condition cleared");
                  Map<Boolean, List<Order>> groupByOrderType =   Stream.generate(Order::nextOrder).limit(100).collect(Collectors.groupingBy(Order::isBuyOrSell));
               buyOrdersList = groupByOrderType.get(true);
              sellOrderrsList = groupByOrderType.get(false);
                Collections.sort(buyOrdersList,new PriceComparator());
                Collections.sort(sellOrderrsList,new PriceComparator());
                  try {
                      Thread.sleep(10);
                  } catch (InterruptedException e) {
                      e.printStackTrace();
                  }
                cond2.signal();

                System.out.println("in two" + buyerCounter.incrementAndGet());
                buylock.unlock();



        }};



             /*  // Stream.generate(Order::nextOrder).limit(100).filter(i -> i.isBuyOrSell()).collect(Collectors.toCollection(()->buyOrdersList));
              //  Stream.generate(Order::nextOrder).limit(100).filter(i -> !i.isBuyOrSell()).collect(Collectors.toCollection(()->sellOrderrsList));
                Stream.generate(()->new PriorityQueue<Order>(buyOrdersList).poll()).limit(50).collect(Collectors.toCollection(()->sellOrderrsList));

                Stream.generate(buyPQueue::poll).collect(Collectors.toCollection(()->buyOrdersList));
                Order myOrder = Order.nextOrder();
                if (myOrder.isBuyOrSell()) {
                    buyPQueue.add(myOrder);
                    buyOrdersList.add(buyPQueue.poll());


                }
                else {
                    sellPQueue.add(myOrder);
                    buyOrdersList.add(sellPQueue.poll());
                    System.out.println("Sell queue size is : " + sellPQueue.size());
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }*/








        Runnable matcher = ()-> {

            while(true) {
                try {

                    buylock.lock();
                    cond2.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

               // System.out.println("Total Buy orders : " + buyOrdersList.size()  +     buyOrdersList);

               //System.out.println("Total Sell orders : " + sellOrderrsList.size()  +     sellOrderrsList);

             //  System.out.println("Now clearing both the list");


                buyOrdersList.clear();
                sellOrderrsList.clear();
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                cond3.signal();
                System.out.println("in three" + buyerCounter.incrementAndGet());
                buylock.unlock();

            }};

        Runnable reporter = ()-> {

            while(true) {
                try {
                  //  System.out.println("in four entry");
                    buylock.lock();
                  //  System.out.println("4 lock acquired");
                    cond3.await();
                  //  System.out.println("4 lock condition cleared");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

               // System.out.println("We are in buy reporter: " + buyOrdersList.size()  +     buyOrdersList);

               // System.out.println("We are in sell reporter " + sellOrderrsList.size()  +     sellOrderrsList);

                //System.out.println("reporting done");


                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                cond4.signal();
                System.out.println("in four" + buyerCounter.incrementAndGet());
                buylock.unlock();

            }};


        Runnable initializer = ()-> {


          while(tracker){
           // System.out.println("in initializer");
              try {
                  Thread.sleep(10);
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
              buylock.lock();


              System.out.println("in one" + buyerCounter.incrementAndGet());
              try {
                  Thread.sleep(1000);
              } catch (InterruptedException e) {
                  e.printStackTrace();
              }
              cond4.signal();
              tracker=false;
              buylock.unlock();



           // System.out.println("leaving in initializer");

           }};

        new Thread(initializer).start();
        new Thread(orderReceiver).start();
        new Thread(matcher).start();
       new Thread(reporter).start();






       /* PriorityQueue <Integer> myqueue = new PriorityQueue<>();
        myqueue.add(1);
        myqueue.add(6);
        myqueue.add(2); myqueue.add(3);
        myqueue.add(1);
        myqueue.add(9);
        myqueue.add(4);
        myqueue.add(11); myqueue.add(3);
        System.out.println(myqueue);
        myqueue.poll();
        System.out.println(myqueue);
        myqueue.poll();
        System.out.println(myqueue);
        myqueue.poll();
        System.out.println(myqueue);
        myqueue.poll();
        System.out.println(myqueue);
        myqueue.poll();
        System.out.println(myqueue);myqueue.poll();
        System.out.println(myqueue);*/

      /*  //ArrayList <Integer> randomList = new ArrayList<Integer>();
        long start= System.currentTimeMillis();
        Queue<Integer> result1= random.ints( 100000000,100,1000000000).collect( PriorityQueue::new,PriorityQueue::add,PriorityQueue::addAll);
        Integer result11 = random.ints( 100000000,100,1000000000).limit(100).reduce(Integer::sum).getAsInt();

        result1.stream().limit(100).reduce(Integer::sum);

        //System.out.println(result1);
        long end= System.currentTimeMillis();
        System.out.println(end-start);*
        //Thread.sleep(5000);
       // Stream.generate(result1::poll).limit(result1.size()).forEach(System.out::println);





       /* Runnable myrunnable = () -> {
            System.out.println("I am running");};

        Callable <Integer> mycallable = () -> {
            System.out.println("I am callable");
            Thread.sleep(10000);
            if(1==1)
            throw new Exception("");
        return 5;};

        new Thread(myrunnable).start();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future future = executorService.submit(mycallable);
       System.out.println(future.isDone());
       future.get();
        executorService.shutdown();

        Supplier <String> mysupplier = () -> {
            System.out.println("I am running");
            try {
                Thread.sleep(10000);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "Pramod";
        };
        CompletableFuture<String> future1
                = CompletableFuture.supplyAsync(mysupplier);
        CompletableFuture<String> future2
                = CompletableFuture.supplyAsync(mysupplier);
        CompletableFuture<String> future3
                = CompletableFuture.supplyAsync(() -> "World");

        CompletableFuture<Void> combinedFuture
                = CompletableFuture.allOf(future1, future2, future3);

// ...
Thread.yield();
        System.out.println(future1.isDone());
        System.out.println(future2.isDone());
        System.out.println(future3.isDone());
        System.out.println(future1.get());
        System.out.println(future2.get());
        System.out.println(future3.get());

        System.out.println(combinedFuture.get());
*/
        /*assertTrue(future1.isDone());
        assertTrue(future2.isDone());
        assertTrue(future3.isDone());*/
    }

    public static void main11(String[] args) throws IOException, ClassNotFoundException {
        AppleProduct macBook = new AppleProduct();
        macBook.headphonePort = "headphonePort2020";
        macBook.thunderboltPort = "thunderboltPort2020";

        String serializedObj = serializeObjectToString(macBook);

        System.out.println("Serialized AppleProduct object to string:");
        System.out.println(serializedObj);


        System.out.println(
                "Deserializing AppleProduct...");

        AppleProduct deserializedObj = (AppleProduct) deSerializeObjectFromString(
                serializedObj);

        System.out.println(
                "Headphone port of AppleProduct:"
                        + deserializedObj.getHeadphonePort());
        System.out.println(
                "Thunderbolt port of AppleProduct:"
                        + deserializedObj.getThunderboltPort());

    }

    public static Object deSerializeObjectFromString(String s)
            throws IOException, ClassNotFoundException {

        byte[] data = Base64.getDecoder().decode(s);
        ObjectInputStream ois = new ObjectInputStream(
                new ByteArrayInputStream(data));
        Object o = ois.readObject();
        ois.close();
        return o;
    }

    public static String serializeObjectToString(Serializable o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();

        return Base64.getEncoder().encodeToString(baos.toByteArray());
    }


    public static void main2(String[] args) throws InterruptedException {

        ExecutorService executor = Executors.newSingleThreadExecutor();

        Future<?> future = executor.submit(() -> {

            printNumbers(); // first call

            printNumbers(); // second call

        });

        Thread.sleep(8_000);

        executor.shutdownNow();  // will interrupt the task

        executor.awaitTermination(6, TimeUnit.SECONDS);

    }

    private static void printNumbers() {

        for (int i = 0; i <10; i++) {

            System.out.print(i);

            //Thread.sleep(1_000);
            System.out.println(Thread.currentThread().isInterrupted());

        }

    }




    public static void main1(String[] args) throws InterruptedException {
        System.out.println("Hello");

        Runnable myrunnable = () -> {
            System.out.println("I am running");};

            Thread.sleep(1000);

            new Thread(myrunnable).interrupt();
        ArrayList<String> list  = new ArrayList();



        PriorityQueue<Integer> myQueue = new PriorityQueue<>(Collections.reverseOrder());
        IntStream.range(1,10).forEach(item -> myQueue.add(item));

        HashMap<String,Integer> map = new HashMap<>(1000);
        map.put("askf",1);
        map.put("laksd",44);
        Supplier <HashMap<String,Integer>> mysuppier = (Supplier<HashMap<String, Integer>>) map;
        Stream.generate(mysuppier).forEach(System.out::println);

        Map<Integer, String> hmap = new HashMap<Integer, String>();
        hmap.put(11, "Apple");
        hmap.put(22, "Orange");
        hmap.put(33, "Kiwi");
        hmap.put(44, "Banana");

        Map<Integer, String> result = hmap.entrySet()

                .stream()
                .filter(ma -> ma.getKey().intValue() <= 22)
                .collect(Collectors.toMap(ma -> ma.getKey(), ma -> ma.getValue()));

        System.out.println("Result: " + result);

        System.out.println("myQueue " + myQueue);
      //  List<Integer> result = Stream.generate(myQueue::poll)
         //       .limit(myQueue.size())
       //         .collect(Collectors.toList());
        Stream.generate(myQueue::poll).limit(myQueue.size()).forEach(System.out::println);
        //       .limit(myQueue.size())
        //         .collect(Collectors.toList());

        }
    }