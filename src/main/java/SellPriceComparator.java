import java.util.Comparator;

public class SellPriceComparator implements Comparator <Order>{

    @Override
    public int compare(Order first, Order sec){
        if (first.getPrice()<sec.getPrice())
            return 1;
        if (first.getPrice()> sec.getPrice())
            return -1;
        if (first.getPrice()==sec.getPrice())
            return first.getTime().compareTo(sec.getTime());
        else
            return -1;
    }
}
