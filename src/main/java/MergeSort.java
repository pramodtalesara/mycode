import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class MergeSort {

    void merge(int arr[], int left, int mid, int right) {


        System.out.println("merging " + left + " to " + mid + " and " + mid + "to " + right );
        int len1 = mid - left + 1;
        int len2 = right - mid;

        int leftArr[] = new int[len1];
        int rightArr[] = new int[len2];


        for (int i = 0; i < len1; i++)
            leftArr[i] = arr[left + i];
        for (int j = 0; j < len2; j++)
            rightArr[j] = arr[mid + 1 + j];
        System.out.println("Array befor merging");
        display(leftArr);
        display(rightArr);
        int i, j, k;
        i = 0;
        j = 0;
        int o=0;
        k = left;

        while (i < len1 && j < len2) {
            if (leftArr[i] <= rightArr[j]) {
                arr[k] = leftArr[i];
                o++;
                i++;
            } else {
                arr[k] = rightArr[j];
                j++;
                o++;
            }
            k++;
        }

        while (i < len1) {
            arr[k] = leftArr[i];
            i++;
            k++;
            o++;
        }

        while (j < len2) {
            arr[k] = rightArr[j];
            j++;
            k++;
            o++;
        }
        System.out.println("Array aftrer merging");
        for (int n = left; n < o; ++n){
            System.out.print(arr[n] + " ");
        }
        System.out.println();

    }

    void mergeSort(int arr[], int start, int right) {

        if (start < right) {
            System.out.println("Dispya array before breakup");
            display(arr,start,right);

            int mid = (start + right) / 2;

            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, right);

            merge(arr, start, mid, right);
        }
    }

    static void display(int arr[]) {
        int n = arr.length;
        for (int i = 0; i < n; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }


    static void display(int arr[], int start, int end) {
       // int n = arr.length;
        for (int i = start; i <= end; ++i)
            System.out.print(arr[i] + " ");
        System.out.println();
    }
    public static void main(String args[]) {
        Random r = new Random();
        int[] arr = IntStream.generate(() -> r.nextInt(1000000)).limit(10000).toArray();

        MergeSort ob = new MergeSort();

       /* System.out.println("Original array");
        display(arr);*/

        ob.mergeSort(arr, 0, arr.length - 1);

        System.out.println("Sorted array");
        display(arr);
    }





    public static List<Long> pmerge(List<Long> a, List<Long> b) {
        int i=0, j=0;
        List<Long> result = new ArrayList<>(a.size() + b.size());
        while(i < a.size() && j < b.size())
            result.add(a.get(i) < b.get(j) ? a.get(i++): b.get(j++));

        while(i < a.size())
            result.add(a.get(i++));

        while(j < b.size())
            result.add(b.get(j++));

        return result;
    }

    public static long[] pmerge(long[] a, long[] b) {

        long[] answer = new long[a.length + b.length];
        int i = 0, j = 0, k = 0;

        while(i < a.length && j < b.length)
            answer[k++] = a[i] < b[j] ? a[i++] : b[j++];

        while(i < a.length)
            answer[k++] = a[i++];

        while(j < b.length)
            answer[k++] = b[j++];

        return answer;
    }
}

  class MergeTask extends RecursiveTask<List<Long>> {
    private static final int THRESHOLD = 4;
    private final List<Long> list;

    public MergeTask(List<Long> list) {
        this.list = list;
    }

    @Override
    protected List<Long> compute() {
        if (list.size() < THRESHOLD) {
            return list.stream().sorted().collect(Collectors.toList());
        }

        MergeTask left = new MergeTask(list.stream().limit(list.size()/2).collect(Collectors.toList()));
        MergeTask right = new MergeTask(list.stream().skip(list.size()/2).collect(Collectors.toList()));
        invokeAll(left, right);

        return MergeSort.pmerge(left.join(), right.join());
    }
}