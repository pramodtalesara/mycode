import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.function.Supplier;

public class Order  {
    static int orderSeq=10000;
    Boolean buyOrSell;
    LocalTime time;
    int quantity;
    String scriptCode;
    float price;
    int filled;
    int orderID;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return buyOrSell == order.buyOrSell && quantity == order.quantity && Float.compare(order.price, price) == 0 && filled == order.filled && orderID == order.orderID && time.equals(order.time) && scriptCode.equals(order.scriptCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(buyOrSell, time, quantity, scriptCode, price, filled, orderID);
    }

    public Boolean isBuyOrSell() {
        return buyOrSell;
    }

    public void setBuyOrSell(boolean buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getScriptCode() {
        return scriptCode;
    }

    public void setScriptCode(String scriptCode) {
        this.scriptCode = scriptCode;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getFilled() {
        return filled;
    }

    public void setFilled(int filled) {
        this.filled = filled;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Order(boolean buyOrSell, LocalTime time, int quantity, String scriptCode, float price, int filled, int orderId) {
        this.buyOrSell = buyOrSell;

        this.quantity = quantity;
        this.scriptCode = scriptCode;
        this.price = price;
        this.filled = filled;
        this.orderID=1111111;
        this.time=LocalTime.now();;
        LocalDateTime now = LocalDateTime.now();
        LocalTime nowT = LocalTime.now();


           }

    @Override
    public String toString() {
        return "{" +
                  "price=" + price +
                '}';
    }

    public Order(Boolean buyOrSell) {
        this.buyOrSell = buyOrSell;
    }


    static Order nextOrder(){
        Random random = new Random();
        random.nextBoolean();
        int low = 401;
        int high = 450;
        int  price = random.nextInt(high-low) + low;
        int quantity = random.nextInt(1000);
        return new Order(random.nextBoolean(),LocalTime.now(),quantity,"ABC",price,0,++orderSeq);

    }




}
