import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class MyExchange {

    public ReentrantLock buylock = new ReentrantLock(true);
    public ReentrantReadWriteLock readWrite = new ReentrantReadWriteLock(true);
    ReentrantReadWriteLock.ReadLock readlock = readWrite.readLock();
    ReentrantReadWriteLock.WriteLock writeLock = readWrite.writeLock();

    public Condition cond1 = buylock.newCondition();
    public Condition cond2 = buylock.newCondition();
    public Condition cond3 = buylock.newCondition();
    public Condition cond4 = buylock.newCondition();
    public AtomicInteger counter = new AtomicInteger();
    public boolean tracker = true;
    public BlockingQueue<String> myBQ = new ArrayBlockingQueue<String>(5);
    public Semaphore mySemaphore = new Semaphore(4);

    public void giveToTwo() {

        while (tracker) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buylock.lock();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("ine One " + counter.incrementAndGet());
            cond2.signal();
            tracker = false;
            buylock.unlock();


        }


    }


    public void giveToThree() {

        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buylock.lock();
            try {
                cond2.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("in two" + counter.incrementAndGet());
            cond3.signal();
            buylock.unlock();


        }

    }

    public void firstRead() {
        while (true) {

            readlock.lock();


            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Exiting first read");
            readlock.unlock();


        }

    }

    public void secoundtRead() {
        while (true) {

            readlock.lock();


            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Exiting sec read");
            readlock.unlock();


        }

    }

    public void thirdRead() {
        while (true) {

            readlock.lock();
            System.out.println("In third read");

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Exiting third read");
            readlock.unlock();


        }

    }

    public void firstWrite() {
        while (true) {

            writeLock.lock();
            System.out.println("Now I have write lock  write");

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("No thread entered read as I was writeing...now existing so some read can enter");
            writeLock.unlock();


        }

    }

    public void giveToFour() {

        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buylock.lock();
            try {
                cond3.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("In three" + counter.incrementAndGet());
            cond4.signal();
            buylock.unlock();


        }
    }

    public void giveToFive() {

        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            buylock.lock();
            try {
                cond4.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("In four" + counter.incrementAndGet());
            cond2.signal();
            buylock.unlock();
        }
    }


    public void take() {

        while (true) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                System.out.println("taken " + myBQ.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void add() {

        while (true) {
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String value = new Random().nextInt() + System.currentTimeMillis() + "";
            try {
                myBQ.put(value);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("added " + value);
            try {
                Thread.sleep(0);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void mySemaphoneCode1() {

        try {
            mySemaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void mySemaphoneCode2() {

        try {
            mySemaphore.acquire();
            mySemaphore.availablePermits();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) {

        MyExchange exc = new MyExchange();

        Runnable one = () -> {
            exc.giveToTwo();


        };


        Runnable two = () -> {
            exc.giveToThree();


        };

        Runnable three = () -> {
            exc.giveToFour();


        };

        Runnable four = () -> {
            exc.giveToFive();


        };

        Runnable firstRead = () -> {
            exc.firstRead();


        };

        Runnable secoundRead = () -> {
            exc.secoundtRead();


        };

        Runnable thirdRead = () -> {
            exc.thirdRead();


        };
        Runnable firstWrite = () -> {
            exc.firstWrite();


        };
        Runnable takeFromQueue = () -> {
            exc.take();


        };
        Runnable addToQueue = () -> {
            exc.add();

            ScheduledExecutorService scheduledExecutorService =
                    Executors.newScheduledThreadPool(5);

            ScheduledFuture scheduledFuture =
                    scheduledExecutorService.schedule(new Callable() {
                                                          public Object call() throws Exception {
                                                              System.out.println("Executed!");
                                                              return "Called!";
                                                          }
                                                      },
                            5,
                            TimeUnit.SECONDS);


        };

       /* new Thread(one).start();

        new Thread(two).start();

        new Thread(three).start();

      *//*  new Thread(four).start();
        new Thread(firstRead).start();
        new Thread(secoundRead).start();
        new Thread(thirdRead).start();
        new Thread(firstWrite).start();*/
        //  new Thread(addToQueue).start();
        //  new Thread(takeFromQueue).start();
        Runnable task1 = () -> {
            int i = 1000;
            while (i > 1) {
                i--;
                System.out.println("In thread " + Thread.currentThread() + " value of i: " + i);
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        };
        ExecutorService executor = Executors.newFixedThreadPool(8);
        IntStream.range(1, 2).forEach((a) -> executor.execute(task1));
        executor.shutdown();

        Executors.newCachedThreadPool();
        Executors.newScheduledThreadPool(2);
        Executors.newScheduledThreadPool(2);
        Executors.newSingleThreadExecutor();


        for (; ; ) {
            System.out.println("what is this");
            try {
                Thread.sleep(2222);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // creating a Semaphore object
      /*  // with number of permits 1
        Semaphore sem = new Semaphore(2);

        // creating two threads with name A and B
        // Note that thread A will increment the count
        // and thread B will decrement the count
        MyThread1 mt1 = new MyThread1(sem, "A");
        MyThread1 mt2 = new MyThread1(sem, "B");
       System.out.println("No of core" + Runtime.getRuntime().availableProcessors());
*/
            // stating threads A and B
            // mt1.start();
            //   mt2.start();

      /*  // waiting for threads A and B
        try {
            mt1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            mt2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // count will always remain 0 after
        // both threads will complete their execution
        System.out.println("count: " + Shared.count);*/
        }


    }


}









class Shared
{
    static int count = 0;
}
class MyThread1 extends Thread {
    Semaphore sem;
    String threadName;

    public MyThread1(Semaphore sem, String threadName) {
        super(threadName);
        this.sem = sem;
        this.threadName = threadName;
    }

    @Override
    public void run() {

        // run by thread A
        if (this.getName().equals("A")) {
            System.out.println("Starting " + threadName);
            try {


                // First, get a permit.
                System.out.println(threadName + " is waiting for a permit.");

                // acquiring the lock
                sem.acquire();

                System.out.println(threadName + " gets a permit.");

                // Now, accessing the shared resource.
                // other waiting threads will wait, until this
                // thread release the lock
                for (int i = 0; i < 10; i++) {
                    Shared.count++;
                    System.out.println(threadName + ": " + Shared.count);

                    // Now, allowing a context switch -- if possible.
                    // for thread B to execute
                    Thread.sleep(100);
                }
            } catch (InterruptedException exc) {
                System.out.println(exc);
            }

            // Release the permit.
            System.out.println(threadName + " releases the permit.");
            sem.release();
        }

        // run by thread B
        else {
            System.out.println("Starting " + threadName);
            try {

                System.out.println(threadName + " is waiting for a permit.");


                // acquiring the lock
                sem.acquire();

                System.out.println(threadName + " gets a permit.");

                // Now, accessing the shared resource.
                // other waiting threads will wait, until this
                // thread release the lock
                for (int i = 0; i < 10; i++) {
                    Shared.count--;
                    System.out.println(threadName + ": " + Shared.count);

                    // Now, allowing a context switch -- if possible.
                    // for thread A to execute
                    Thread.sleep(100);
                }
            } catch (InterruptedException exc) {
                System.out.println(exc);
            }
            // Release the permit.
            System.out.println(threadName + " releases the permit.");
            sem.release();
        }
    }

}


class CustomRecursiveTask extends RecursiveTask<Integer> {
    private int[] arr;

    private static final int THRESHOLD = 20;

    public CustomRecursiveTask(int[] arr) {
        this.arr = arr;
    }

    @Override
    protected Integer compute() {
        if (arr.length > THRESHOLD) {
            return ForkJoinTask.invokeAll(createSubtasks())
                    .stream()
                    .mapToInt(ForkJoinTask::join)
                    .sum();
        } else {
            return processing(arr);
        }
    }

    private Collection<CustomRecursiveTask> createSubtasks() {
        List<CustomRecursiveTask> dividedTasks = new ArrayList<>();
        dividedTasks.add(new CustomRecursiveTask(
                Arrays.copyOfRange(arr, 0, arr.length / 2)));
        dividedTasks.add(new CustomRecursiveTask(
                Arrays.copyOfRange(arr, arr.length / 2, arr.length)));
        return dividedTasks;
    }

    private Integer processing(int[] arr) {
        return Arrays.stream(arr)
                .filter(a -> a > 10 && a < 27)
                .map(a -> a * 10)
                .sum();
    }
}

class CustomRecursiveAction extends RecursiveAction {

    private String workload = "";
    private static final int THRESHOLD = 4;

    private static Logger logger =
            Logger.getAnonymousLogger();

    public CustomRecursiveAction(String workload) {
        this.workload = workload;
    }

    @Override
    protected void compute() {
        if (workload.length() > THRESHOLD) {
            ForkJoinTask.invokeAll(createSubtasks());
        } else {
            processing(workload);
        }
    }

    private List<CustomRecursiveAction> createSubtasks() {
        List<CustomRecursiveAction> subtasks = new ArrayList<>();

        String partOne = workload.substring(0, workload.length() / 2);
        String partTwo = workload.substring(workload.length() / 2, workload.length());

        subtasks.add(new CustomRecursiveAction(partOne));
        subtasks.add(new CustomRecursiveAction(partTwo));

        return subtasks;
    }

    private void processing(String work) {
        String result = work.toUpperCase();
        logger.info("This result - (" + result + ") - was processed by "
                + Thread.currentThread().getName());
    }
}
